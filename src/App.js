import "./App.css";
import logo from "../src/assests/logo.jpg";
import VideoCard from "./VideoCard";
import one from "../src/assests/one.mp4";
import mobileapple from "../src/assests/mobileapple.png";
function App() {
  return (
    <div className="app">
      <div className="app__top">
        {/* logo */}
        <img className="app__logo" src={logo} />
        {/* logo title */}
        <h1>Reels</h1>
      </div>
      <div className="app__videos">
        {/* Container of the video */}
        <VideoCard
          channel="cleverquiz"
          avatarSrc={mobileapple}
          song="Test Song"
          url={one}
          likes={950}
          shares={30}
        />
        <VideoCard />
        <VideoCard />
        <VideoCard />
        <VideoCard />
        <VideoCard />

        {/* Video */}
        {/* Video */}
        {/* Video */}
        {/* Video */}
        {/* Video */}
        {/* Video */}
      </div>
    </div>
  );
}

export default App;
