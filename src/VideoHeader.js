import React from "react";
import "./VideoHeader.css";
// import ArrowBackIcon from "@mui/icons-material/ArrowBack";
// import CameraAltIcon from "@mui/icons-material/CameraAlt";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { CameraAltOutlined } from "@material-ui/icons";
function VideoHeader() {
  return (
    <div className="videoHeader">
      <ArrowBackIosIcon />
      <h3>Reels</h3>
      {/* <h3>Reels</h3> */}
      <CameraAltOutlined />
      {/* <MenuIcon /> */}
      {/* <svg data-testid="ArrowBackIcon"></svg> */}
    </div>
  );
}

export default VideoHeader;
